### Prerequisite
- Java 8+

### How to build the application?
On Windows

`mvnw.bat clean build`

On *nix

`./mvnw clean build`

### How to run the application?
On Windows

`mvnw.bat bootRun`

On *nix

`./mvnw bootRun`

### Access and play around the APIs
Go to http://localhost:9999/swagger-ui.html.

### Get the OpenAPI spec on the fly
You could retrieve the OpenAPI doc in http://localhost:9999/api-docs or you could check the generated OpenAPI document in [openapi-on-the-fly.json](generated-open-api-sepc/openapi-on-the-fly.json)

### Get the OpenAPI spec in Maven build
Run

`./mvnw clean verify`

You could retrieve the OpenAPI doc in target/openapi-by-maven.json or you could check the [openapi-by-maven.json](generated-open-api-sepc/openapi-by-maven.json)

Beware that this step would start the app in the Maven integration-test phase