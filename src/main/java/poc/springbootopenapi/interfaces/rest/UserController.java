package poc.springbootopenapi.interfaces.rest;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.headers.Header;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import poc.springbootopenapi.interfaces.rest.model.CreateUserRequest;
import poc.springbootopenapi.interfaces.rest.model.User;

@RestController("/users/")
public class UserController {
    @PostMapping(consumes = {MediaType.APPLICATION_JSON_VALUE})
    @Operation(description = "Create new User")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "201",
                    description = "The user is created successfully",
                    headers = {@Header(name = "Location", description = "The resource path to new created user")}
            ),
            @ApiResponse(
                    responseCode = "400",
                    description = "Validation error"
            )
    })
    public ResponseEntity<Void> createUser(@RequestBody CreateUserRequest createUserRequest) {
        throw new UnsupportedOperationException("not implemented yet");
    }

    @GetMapping(value = "{userId}", produces = {MediaType.APPLICATION_JSON_VALUE})
    @Operation(description = "Retrieve a user by user id")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "a user"
            ),
            @ApiResponse(
                    responseCode = "404",
                    description = "The user does not exist"
            )
    })
    public ResponseEntity<User> findUser(@PathVariable("userId") Long userId) {
        throw new UnsupportedOperationException("not implemented yet");
    }

    @GetMapping(produces = {MediaType.APPLICATION_JSON_VALUE})
    @Operation(description = "Retrieve a list of user")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "list of user"
            )
    })
    public ResponseEntity<Page<User>> findUsers(Pageable pageable) {
        throw new UnsupportedOperationException("not implemented yet");
    }
}
