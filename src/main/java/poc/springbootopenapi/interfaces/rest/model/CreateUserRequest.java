package poc.springbootopenapi.interfaces.rest.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Schema(name = "CreateUserRequest", description = "request model for creating user")
@Data
public class CreateUserRequest {
    @Schema(required = true)
    private String name;

    @Schema(required = true)
    private Integer age;
}
